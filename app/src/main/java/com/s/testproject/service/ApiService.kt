package com.s.testproject.service


import com.s.testproject.datamodel.base.MyResponse
import com.s.testproject.datamodel.body.ContentBdy
import com.s.testproject.datamodel.body.DetailsContentBody
import com.s.testproject.datamodel.response.ContentResponse
import com.s.testproject.datamodel.response.DetailsContentResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST


interface ApiService {

    companion object {
        const val BASE_URL = "https://core.gapfilm.ir/mobile/request.asmx/"
    }

    @POST("GetContentList")
    fun getContent(@Body contentBdy: ContentBdy): Single<MyResponse<ContentResponse>>

    @POST("GetContent")
    fun getDetailsContent(@Body detailsContentBody: DetailsContentBody): Single<MyResponse<DetailsContentResponse>>
}