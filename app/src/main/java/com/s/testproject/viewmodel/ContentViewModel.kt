package com.s.testproject.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.s.testproject.datamodel.base.Response
import com.s.testproject.datamodel.response.ContentResponse
import com.s.testproject.repository.ContentRepository

/**
 * Created by Mostafa Shiri.
 */
class ContentViewModel(private val contentRepository: ContentRepository) : ViewModel(){

    var content = MutableLiveData<Response<ContentResponse>>()

    fun fetchContentList(page: Int) {
        contentRepository.getContents(page)
        content = contentRepository.content
    }

    fun getContents(): MutableLiveData<Response<ContentResponse>>? {
        return content
    }

    override fun onCleared() {
        super.onCleared()
        contentRepository.disposable?.dispose()
    }
}