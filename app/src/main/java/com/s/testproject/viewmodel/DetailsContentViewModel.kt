package com.s.testproject.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.s.testproject.datamodel.base.Response
import com.s.testproject.datamodel.response.ContentResponse
import com.s.testproject.datamodel.response.DetailsContentResponse
import com.s.testproject.repository.ContentRepository
import com.s.testproject.repository.DetailsContentRepository

/**
 * Created by Mostafa Shiri.
 */
class DetailsContentViewModel(private val detailsContentRepository: DetailsContentRepository) : ViewModel(){

    var detailsContent = MutableLiveData<Response<DetailsContentResponse>>()

    fun fetchDetailsContent(id: Int) {
        detailsContentRepository.getDetailsContent(id)
        detailsContent = detailsContentRepository.detailsContent
    }

    fun getDetailsContents(): MutableLiveData<Response<DetailsContentResponse>>? {
        return detailsContent
    }

    override fun onCleared() {
        super.onCleared()
        detailsContentRepository.disposable?.dispose()
    }
}