package com.s.testproject.di

import com.s.testproject.viewmodel.ContentViewModel
import com.s.testproject.viewmodel.DetailsContentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { ContentViewModel(get()) }
    viewModel { DetailsContentViewModel(get()) }
}