package com.s.testproject.di

import com.s.testproject.view.adapter.ContentAdapter
import org.koin.dsl.module

/**
 * Created by Mostafa Shiri.
 */

val adapterModule = module {
    factory{ ContentAdapter()}
}