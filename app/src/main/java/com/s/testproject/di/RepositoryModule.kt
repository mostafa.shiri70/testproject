package com.s.testproject.di

import com.s.testproject.repository.ContentRepository
import com.s.testproject.repository.DetailsContentRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory{ ContentRepository(get()) }
    factory{ DetailsContentRepository(get()) }
}