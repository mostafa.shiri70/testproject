package com.s.testproject.core

import androidx.multidex.MultiDexApplication
import com.s.testproject.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

/**
 * Created by Mostafa Shiri.
 */
class TestApp: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@TestApp)
            androidLogger(Level.ERROR)
            modules(listOf(netModule, apiModule, repositoryModule, viewModelModule, adapterModule))
        }
    }

}