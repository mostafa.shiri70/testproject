package com.s.testproject.core


import java.io.Serializable
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

/**
 * Created by Mostafa Shiri.
 */

class TabHistory : Serializable {

    private val stackNew: HashSet<Int> = HashSet()
    private var stack: ArrayList<Int> = ArrayList()
    var list = HashSet<Int>()

    private val isEmpty: Boolean
        get() = stack.size == 0

    val size: Int
        get() = stack.size

    fun push(entry: Int) {
        if (!stack.contains(entry))
            stack.add(entry)
    }

    fun popPrevious(): Int {
        var entry = -1
        if (!isEmpty) {
            entry = stack[stack.size - 2]
            stack.removeAt(stack.size - 2)
            //entry=stack.indexOf(stack.size-2)
            //stack.remove(stack.size - 2)
        }
        return entry
    }

    fun clear() {
        stack.clear()
    }

}