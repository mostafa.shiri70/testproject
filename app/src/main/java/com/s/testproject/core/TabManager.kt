package com.s.testproject.core

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isInvisible
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.s.testproject.R
import com.s.testproject.view.MainActivity
import kotlinx.android.synthetic.main.activity_main.*


/**
 * Created by Mostafa Shiri.
 */

class TabManager(private val mainActivity: MainActivity){

    private val startDestinations = mapOf(
        R.id.navigation_content to R.id.content_frg,
        R.id.navigation_favorite to R.id.favorite_frg)

    private var currentTabId: Int = R.id.navigation_content
    var currentController: NavController? = null

    private var tabHistory = TabHistory().apply {
        push(R.id.navigation_content)
    }

     val navContentController: NavController by lazy {
        mainActivity.findNavController(R.id.content_tab).apply {
            graph = navInflater.inflate(R.navigation.navigation_bottom).apply {
                startDestination = startDestinations.getValue(R.id.navigation_content)
            }
        }
    }

    private val navFavoriteController: NavController by lazy {
        mainActivity.findNavController(R.id.favorite_tab).apply {
            graph = navInflater.inflate(R.navigation.navigation_bottom).apply {
                startDestination = startDestinations.getValue(R.id.navigation_favorite)
            }
        }
    }

    private val contentTabContainer: View by lazy { mainActivity.contentTabContainer }
    private val favoriteTabContainer: View by lazy { mainActivity.favoriteTabContainer }

    fun onSaveInstanceState(outState: Bundle?) {
        outState?.putSerializable(KEY_TAB_HISTORY, tabHistory)
    }

    fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        savedInstanceState?.let {
           // tabHistory = it.getSerializable(KEY_TAB_HISTORY) as TabHistory
            switchTab(mainActivity.bottomNavigationView.selectedItemId, false)
        }
    }

    fun supportNavigateUpTo(upIntent: Intent) {
        currentController?.navigateUp()
    }

    fun onBackPressed() {
        currentController?.let {
            if (it.currentDestination == null || it.currentDestination?.id == startDestinations.getValue(currentTabId)) {
                if (tabHistory.size > 1) {
                    val tabId = tabHistory.popPrevious()
                    switchTab(tabId, false)
                    mainActivity.bottomNavigationView.menu.findItem(tabId)?.isChecked = true
                } else {
                    mainActivity.finish()
                }
            }
            it.popBackStack()
        } ?: run {
            mainActivity.finish()
        }
    }

    fun switchTab(tabId: Int, addToHistory: Boolean = true) {
        currentTabId = tabId

        when (tabId) {
            R.id.navigation_content -> {
                currentController = navContentController
                invisibleTabContainerExcept(contentTabContainer)
            }
            R.id.navigation_favorite -> {
                currentController = navFavoriteController
                invisibleTabContainerExcept(favoriteTabContainer)
            }

        }
        if (addToHistory) {
            tabHistory.push(tabId)
        }
    }

    private fun invisibleTabContainerExcept(container: View) {
        contentTabContainer.isInvisible = true
        favoriteTabContainer.isInvisible = true

        container.isInvisible = false
    }

    companion object {
        private const val KEY_TAB_HISTORY = "key_tab_history"
    }
}