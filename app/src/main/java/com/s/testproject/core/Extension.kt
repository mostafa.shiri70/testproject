package com.s.testproject.core

import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide



fun ImageView.load(uri: Uri?) {
    if (uri != null) Glide.with(this).load(uri).into(this)
}

fun ImageView.load(url: String?) {
    if (url != null) Glide.with(this).load(url).into(this)
}
