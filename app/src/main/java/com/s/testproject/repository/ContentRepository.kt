package com.s.testproject.repository

import androidx.lifecycle.MutableLiveData
import com.s.testproject.datamodel.body.Request
import com.s.testproject.datamodel.base.MyResponse
import com.s.testproject.datamodel.base.Response
import com.s.testproject.datamodel.base.Response.Companion.error
import com.s.testproject.datamodel.base.Response.Companion.loading
import com.s.testproject.datamodel.base.Response.Companion.success
import com.s.testproject.datamodel.body.ContentBdy
import com.s.testproject.datamodel.response.ContentResponse
import com.s.testproject.service.ApiService
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Mostafa Shiri.
 */

class ContentRepository(private val apiService: ApiService){

    var disposable: Disposable? = null
    val content = MutableLiveData<Response<ContentResponse>>()

    fun getContents(page:Int){
        content.postValue(loading(null))
        val request= Request()
        request.pageIndex=page
        val body=ContentBdy()
        body.request=request
        apiService.getContent(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<MyResponse<ContentResponse>> {
                override fun onSuccess(response: MyResponse<ContentResponse>) {
                    content.postValue(loading(null))
                    if (response.status==1) {
                        content.postValue(success(response.result))
                    }else
                        content.postValue(error(response.message!!,null))

                }

                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onError(e: Throwable) {
                    val a=e.message
                    content.postValue(loading(null))
                    content.postValue(error("http error",null))
                }
            })
    }
}