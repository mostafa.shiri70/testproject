package com.s.testproject.repository

import androidx.lifecycle.MutableLiveData
import com.s.testproject.datamodel.body.Request
import com.s.testproject.datamodel.base.MyResponse
import com.s.testproject.datamodel.base.Response
import com.s.testproject.datamodel.base.Response.Companion.error
import com.s.testproject.datamodel.base.Response.Companion.loading
import com.s.testproject.datamodel.base.Response.Companion.success
import com.s.testproject.datamodel.body.ContentBdy
import com.s.testproject.datamodel.body.DetailsContentBody
import com.s.testproject.datamodel.body.DetailsRequest
import com.s.testproject.datamodel.response.ContentResponse
import com.s.testproject.datamodel.response.DetailsContentResponse
import com.s.testproject.service.ApiService
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Mostafa Shiri.
 */

class DetailsContentRepository(private val apiService: ApiService){

    var disposable: Disposable? = null
    val detailsContent = MutableLiveData<Response<DetailsContentResponse>>()

    fun getDetailsContent(id:Int){
        detailsContent.postValue(loading(null))
        val request= DetailsRequest()
        request.requestId=id
        val body=DetailsContentBody()
        body.request=request
        apiService.getDetailsContent(body)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<MyResponse<DetailsContentResponse>> {
                override fun onSuccess(response: MyResponse<DetailsContentResponse>) {
                    detailsContent.postValue(loading(null))
                    if (response.status==1) {
                        detailsContent.postValue(success(response.result))
                    }else
                        detailsContent.postValue(error(response.message!!,null))

                }

                override fun onSubscribe(d: Disposable) {
                    disposable = d
                }

                override fun onError(e: Throwable) {
                    detailsContent.postValue(loading(null))
                    detailsContent.postValue(error("http error",null))
                }
            })
    }
}