package com.s.testproject.datamodel.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Mostafa Shiri.
 */
class ContentResponse {

    @SerializedName("TotalPages")
    var totalPages: Int? = null

    @SerializedName("GetContentList")
    var getContentList: ArrayList<GetContentList>? = null

    class GetContentList{

        @SerializedName("ContentID")
        var contentID: Int? = null

        @SerializedName("Title")
        var title: String? = null

        @SerializedName("ThumbImage")
        var thumbImage: String? = null

        @SerializedName("FavoriteStatus")
        var favoriteStatus: Boolean? = null

        @SerializedName("ZoneID")
        var zoneID: Int? = null

        @SerializedName("Categories")
        var categories: ArrayList<Categories>? = null

    }

    class Categories{

        @SerializedName("CategoryID")
        var categoryID: Int? = null

        @SerializedName("Title")
        var title: String? = null

        @SerializedName("ParentID")
        var parentID: Int? = null

        @SerializedName("Image")
        var image: String? = null

        @SerializedName("ZoneID")
        var zoneID: Int? = null

        @SerializedName("IsSelected")
        var isSelected: Boolean? = null

        @SerializedName("SectionPriority")
        var sectionPriority: Int? = null

        @SerializedName("IsFollowed")
        var isFollowed: Boolean? = null
    }


}