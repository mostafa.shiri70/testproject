package com.s.testproject.datamodel.base

import com.google.gson.annotations.SerializedName

/**
 * Created by Mostafa Shiri.
 */

open class MyResponse<T> {

    @SerializedName("Status")
    var status: Int? = null

    @SerializedName("Message")
    var message: String? = null

    @SerializedName("Result")
    var result: T? = null
}