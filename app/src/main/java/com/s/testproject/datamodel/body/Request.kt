package com.s.testproject.datamodel.body

/**
 * Created by Mostafa Shiri.
 */
class Request {
        var requestType=2
        var requestId: Int? = null
        private var pageSize=15
        var pageIndex: Int? = null
        var orderBy="createdate"
        var order="desc"
}