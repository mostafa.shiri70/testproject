package com.s.testproject.datamodel.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Mostafa Shiri.
 */
class DetailsContentResponse {


        @SerializedName("Summary")
        var summary: String? = null

        @SerializedName("Title")
        var title: String? = null

        @SerializedName("LandscapeImage")
        var landscapeImage: String? = null

}