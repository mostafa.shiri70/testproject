package com.s.testproject.view.adapter

/**
 * Created by Mostafa Shiri.
 */

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.s.testproject.R
import com.s.testproject.core.load
import com.s.testproject.datamodel.response.ContentResponse
import kotlinx.android.synthetic.main.content_item.view.*

class ContentAdapter:RecyclerView.Adapter<ContentAdapter.MovieHolder>() {

    private val contentsList = mutableListOf<ContentResponse.GetContentList>()

    var adapterOnClick: ((item:ContentResponse.GetContentList) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val inflater = LayoutInflater.from(parent.context)
        return MovieHolder(inflater, parent)
    }

    fun addItems(turnoverList:ArrayList<ContentResponse.GetContentList>) {
        contentsList.addAll(turnoverList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = contentsList.size

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        val itemContent = contentsList[position]
        holder.bindDeal(itemContent)
        holder.setItem(itemContent)
    }
    inner class MovieHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.content_item, parent, false)) {

        fun bindDeal(item: ContentResponse.GetContentList) {
            itemView.tv_title.text = item.title
            itemView.img_film.load(item.thumbImage)
            when(item.zoneID){
                3->{ itemView.tv_film_type.text="سریال"}
                4->{ itemView.tv_film_type.text="سینمایی"}
                else->{ itemView.tv_film_type.text="نامشخص"}
            }
        }

        fun setItem(item: ContentResponse.GetContentList) {
            itemView.setOnClickListener {
                adapterOnClick?.invoke(item)
            }
        }
    }

    fun clear() {
        contentsList.clear()
        notifyDataSetChanged()
    }

}