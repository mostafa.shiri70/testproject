package com.s.testproject.view.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.s.testproject.R
import com.s.testproject.datamodel.base.Status
import com.s.testproject.view.adapter.ContentAdapter
import com.s.testproject.viewmodel.ContentViewModel
import kotlinx.android.synthetic.main.fragment_content.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Mostafa Shiri.
 */

class ContentFragment : Fragment() {

    private val contentViewModel by viewModel<ContentViewModel>()
    private val contentAdapter: ContentAdapter by inject()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRv()
        initObserve()
    }

    private fun initObserve() {
        contentViewModel.fetchContentList(1)

        contentViewModel.getContents()?.observe(requireActivity(), Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    contentAdapter.clear()
                    it.data?.getContentList?.let { it1 -> contentAdapter.addItems(it1) }
                }
                Status.LOADING -> {
                    //Show ProgressBar
                }
                Status.ERROR -> {
                    //Handle Error
                }
            }
        })
    }

    private fun initRv() {
        rv_content.adapter = contentAdapter
        contentAdapter.adapterOnClick = {
                val bundle = bundleOf("ContentID" to it.contentID)
                findNavController().navigate(R.id.action_detail,bundle)
        }
    }
}