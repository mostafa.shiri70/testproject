package com.s.testproject.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.s.testproject.R
import com.s.testproject.core.load
import com.s.testproject.datamodel.base.Status
import com.s.testproject.datamodel.response.DetailsContentResponse
import com.s.testproject.viewmodel.DetailsContentViewModel
import kotlinx.android.synthetic.main.fragment_details_content.*
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * Created by Mostafa Shiri.
 */

class DetailsContentFragment : Fragment() {

    private val detailsContentViewModel by viewModel<DetailsContentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details_content, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getInt("ContentID")?.let { initObserve(it) }
        btn_back.setOnClickListener {
            findNavController().navigate(R.id.action_close_detail, null)
        }
    }

    private fun initObserve(id: Int) {
        detailsContentViewModel.fetchDetailsContent(id)
        detailsContentViewModel.getDetailsContents()?.observe(requireActivity(), Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { it1 -> bindData(it1) }
                }
                Status.LOADING -> {
                    //Show ProgressBar
                }
                Status.ERROR -> {
                    //Handle Error
                }
            }
        })
    }

    private fun bindData(data: DetailsContentResponse) {
        tv_title.text=data.title
        image.load(data.landscapeImage)
        //data.summary?.let { htmlLoader(webView, it) }
        val summary = "<html><body>${data.summary}</body></html>"
        webView.loadData(summary, "text/html", "UTF-8")
    }

    private fun htmlLoader(loader: WebView, html: String) {
        loader.loadData("<div dir='rtl';'>$html</div>", "text/html", "UTF-8")

        val summary = "<html><body>$html</body></html>"
        webView.loadData(summary, "text/html; charset=utf-8", "utf-8")
    }
}